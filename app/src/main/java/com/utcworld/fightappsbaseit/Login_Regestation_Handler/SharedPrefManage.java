package com.utcworld.fightappsbaseit.Login_Regestation_Handler;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefManage {
    private static SharedPrefManage mInstance;
    private static Context mCtx;
    private static final String SHARED_PREF_NAME="mysharedpref12";
    private static final String KEY_USERNAME="username";
    private static final String KEY_USEREMAIL="usereami";
    private static final String KEY_USERPASSWORD="userpassword";
    private static final String KEY_USER_ID="userid";

    private SharedPrefManage(Context context) {
        mCtx = context;
    }

    public static synchronized SharedPrefManage getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SharedPrefManage(context);
        }
        return mInstance;
    }

    public boolean userLogin(int id,String username,String email){
   SharedPreferences sharedPreferences=mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
   SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putInt(KEY_USER_ID,id);
        editor.putString(KEY_USERNAME,username);
        editor.putString(KEY_USEREMAIL,email);
        editor.apply();
        return true;
    }
    public boolean isLogged(){
        SharedPreferences sharedPreferences=mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        if (sharedPreferences.getString(KEY_USERNAME,null)!=null){
            return true;
        }
            return false;
    }

    public boolean logout(){
        SharedPreferences sharedPreferences=mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.clear();
        editor.apply();
        return true;
    }
}