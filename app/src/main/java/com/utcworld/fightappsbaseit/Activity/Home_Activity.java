package com.utcworld.fightappsbaseit.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.utcworld.fightappsbaseit.R;

public class Home_Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_);
    }

    public void Flight_Search(View view) {
        startActivity(new Intent(getApplicationContext(), Flight_Search_Activity.class));
        //finish();
    }
}
