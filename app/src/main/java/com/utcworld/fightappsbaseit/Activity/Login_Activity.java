package com.utcworld.fightappsbaseit.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.utcworld.fightappsbaseit.R;
import com.utcworld.fightappsbaseit.Login_Regestation_Handler.RequestHandler;
import com.utcworld.fightappsbaseit.Login_Regestation_Handler.SharedPrefManage;
import com.utcworld.fightappsbaseit.Splash_Screen.Splash_Screen;
import com.utcworld.fightappsbaseit.configr.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Login_Activity extends AppCompatActivity {
    LinearLayout loginHad,RegeHad,Loginlayout,Regelayout,LoginBt,RegeBT,Regeshow,Loginshow;
    EditText editTextUserName,editTextPassword,editTextEmail;
    EditText LoguserName,Logpassword;

   private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_);
        loginHad=findViewById(R.id.loginhead);
        RegeHad=findViewById(R.id.regehead);
        Loginlayout=findViewById(R.id.loginlayout);
        Regelayout=findViewById(R.id.regilayout);
        LoginBt=findViewById(R.id.loginBT);
        RegeBT=findViewById(R.id.RegestationBT);
        //Loginshow.findViewById(R.id.loginshowbt);
        Regeshow=findViewById(R.id.regeshowbt);
        progressDialog=new ProgressDialog(this);
        //RegiEdit
        editTextUserName=findViewById(R.id.editTxtUsername);
        editTextPassword=findViewById(R.id.editTxtPassword);
        editTextEmail=findViewById(R.id.editTxtEmail);
        //Login Edit
        LoguserName=findViewById(R.id.Logname);
        Logpassword=findViewById(R.id.Logpassword);

    }

    public void signUp(View view) {
        loginHad.setVisibility(View.GONE);
        Loginlayout.setVisibility(View.GONE);
        LoginBt.setVisibility(View.GONE);
        Regeshow.setVisibility(View.GONE);

       // Loginshow.setVisibility(View.VISIBLE);
        RegeHad.setVisibility(View.VISIBLE);
        Regelayout.setVisibility(View.VISIBLE);
        RegeBT.setVisibility(View.VISIBLE);
        //Loginshow.setVisibility(View.VISIBLE);
    }

    public void signLogin(View view) {
        RegeHad.setVisibility(View.GONE);
        Regelayout.setVisibility(View.GONE);
        RegeBT.setVisibility(View.GONE);
       // Loginshow.setVisibility(View.GONE);

        loginHad.setVisibility(View.VISIBLE);
        Loginlayout.setVisibility(View.VISIBLE);
        LoginBt.setVisibility(View.VISIBLE);
        Regeshow.setVisibility(View.VISIBLE);
    }

    public void ButtonResister(View view) {
        final String userName=editTextUserName.getText().toString().trim();
        final String password=editTextPassword.getText().toString().trim();
        final String email=editTextEmail.getText().toString().trim();
        if (userName.isEmpty()) {
            editTextUserName.setError("Field can't be empty");
            return ;
        } else if (userName.length() > 15) {
            editTextUserName.setError("Username too long");
            return ;
        }
        if(password.isEmpty()){
            editTextPassword.setError("Field can't be empty");
            return ;
        }
        if(email.isEmpty()){
            editTextEmail.setError("Field can't be empty");
            return ;
        }
        else {
        UserRegister();
        }

    }
//User Regestion
    public void UserRegister(){
     final String userName=editTextUserName.getText().toString().trim();
    final String password=editTextPassword.getText().toString().trim();
    final String email=editTextEmail.getText().toString().trim();
    progressDialog.setMessage("Registering user....");
    progressDialog.show();


        StringRequest stringRequest=new StringRequest(Request.Method.POST,
                Constants.URL_REGISTER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                      progressDialog.dismiss();
                        try{
                            JSONObject jsonObject = new JSONObject(response);

                                Toast.makeText(Login_Activity.this,jsonObject.getString("message"), Toast.LENGTH_SHORT).show();


                        }catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(Login_Activity.this, "Register Error!" + e.toString(), Toast.LENGTH_SHORT).show();

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                       progressDialog.dismiss();

                                Toast.makeText(Login_Activity.this, "Error "+error, Toast.LENGTH_SHORT).show();
                    }
                } ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();

                params.put("username", userName);
                params.put("password", password);
                params.put("email",email);
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
        //RequestQueue requestQueue= Volley .newRequestQueue(this);
        //requestQueue.add(stringRequest);
    }

    public void LoginBT(View view) {
        final String LoginName=LoguserName.getText().toString().trim();
        final String LoginPassword=Logpassword.getText().toString().trim();
        if (LoginName.isEmpty()) {
            LoguserName.setError("Field can't be empty");
            return ;
        } else if (LoginName.length() > 15) {
            LoguserName.setError("Username too long");
            return ;
        }
         if(LoginPassword.isEmpty()){
             Logpassword.setError("Field can't be empty");
             return ;
         }

        else {
        UserLogin();
        }
    }

    public void UserLogin() {
        final String LoginName=LoguserName.getText().toString().trim();
        final String LoginPassword=Logpassword.getText().toString().trim();
        progressDialog.setMessage("Login user....");
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                Constants.URL_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject obj = new JSONObject(response);
                            if(!obj.getBoolean("error")){
                                SharedPrefManage.getInstance(getApplicationContext())
                                        .userLogin(
                                                obj.getInt("id"),
                                                obj.getString("username"),
                                                obj.getString("email")
                                        );
                                startActivity(new Intent(getApplicationContext(), Home_Activity.class));
                                finish();
                            }else{
                                Toast.makeText(
                                        getApplicationContext(),
                                        obj.getString("message"),
                                        Toast.LENGTH_LONG
                                ).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();

                        Toast.makeText(
                                getApplicationContext(),
                                error.getMessage(),
                                Toast.LENGTH_LONG
                        ).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("username", LoginName);
                params.put("password", LoginPassword);
                return params;
            }

        };

        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }
}
