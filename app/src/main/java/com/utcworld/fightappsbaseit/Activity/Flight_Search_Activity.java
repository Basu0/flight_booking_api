package com.utcworld.fightappsbaseit.Activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.utcworld.fightappsbaseit.R;

public class Flight_Search_Activity extends AppCompatActivity implements View.OnClickListener{

    TextView DateoickerEdit;
    TextView adtValu,chdValu,infValu;
    TextView txtoneway,txtroundtrip,mlroundtrip,Economy,Bussiness,First;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flight__search_);
        DateoickerEdit=findViewById(R.id.DateoickerEdit);

        txtoneway = (TextView)findViewById(R.id.txtoneway);
        txtroundtrip = (TextView)findViewById(R.id.txtroundtrip);
        mlroundtrip = (TextView)findViewById(R.id.txtroundtrip2);

        Economy = (TextView)findViewById(R.id.economy);
        Bussiness = (TextView)findViewById(R.id.bussiness);
        First = (TextView)findViewById(R.id.frist);


        chdValu=findViewById(R.id.chdvalu);
        infValu=findViewById(R.id.infvalu);
         adtValu=findViewById(R.id.adtvalu);
        DateoickerEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDatePicker();
            }
        });

        txtroundtrip.setOnClickListener(this);
        txtoneway.setOnClickListener(this);
        mlroundtrip.setOnClickListener(this);

        Economy.setOnClickListener(this);
        Bussiness.setOnClickListener(this);
        First.setOnClickListener(this);
    }
    public void alertDatePicker() {
        /*
         * Inflate the XML view. activity_main is in res/layout/date_picker.xml
         */
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.date_picker, null, false);

        // the time picker on the alert dialog, this is how to get the value
        final DatePicker myDatePicker = (DatePicker) view.findViewById(R.id.myDatePicker);

        // so that the calendar view won't appear
        myDatePicker.setCalendarViewShown(false);

        // the alert dialog
        new AlertDialog.Builder(Flight_Search_Activity.this).setView(view)
                .setTitle("Set Date")
                .setPositiveButton("Go", new DialogInterface.OnClickListener() {
                    @TargetApi(11)
                    public void onClick(DialogInterface dialog, int id) {


                        int month = myDatePicker.getMonth() + 1;
                        int day = myDatePicker.getDayOfMonth();
                        int year = myDatePicker.getYear();

                        DateoickerEdit.setText(+month + "/" + day + "/" + year);
                        //Toast.makeText(Flight_Search_Activity.this, ""+month + "/" + day + "/" + year, Toast.LENGTH_SHORT).show();
                        //showToast(month + "/" + day + "/" + year);

                        dialog.cancel();

                    }

                }).show();

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.txtroundtrip:

                txtroundtrip.setBackgroundResource(R.drawable.blue_roundshape);
                txtroundtrip.setTextColor(Color.parseColor("#FFFFFF"));
                txtoneway.setTextColor(Color.parseColor("#4b4949"));
                txtoneway.setBackgroundResource(R.drawable.white_roundshape);
                mlroundtrip.setTextColor(Color.parseColor("#4b4949"));
                mlroundtrip.setBackgroundResource(R.drawable.white_roundshape);
                Toast.makeText(this, "RoundWay", Toast.LENGTH_SHORT).show();
                break;

            case R.id.txtoneway:
                Toast.makeText(this, "OneWay", Toast.LENGTH_SHORT).show();
                txtoneway.setBackgroundResource(R.drawable.blue_roundshape);
                txtoneway.setTextColor(Color.parseColor("#FFFFFF"));
                txtroundtrip.setTextColor(Color.parseColor("#4b4949"));
                txtroundtrip.setBackgroundResource(R.drawable.white_roundshape);
                mlroundtrip.setTextColor(Color.parseColor("#4b4949"));
                mlroundtrip.setBackgroundResource(R.drawable.white_roundshape);

                break;
            case R.id.txtroundtrip2:
                mlroundtrip.setBackgroundResource(R.drawable.blue_roundshape);
                mlroundtrip.setTextColor(Color.parseColor("#FFFFFF"));
                txtoneway.setTextColor(Color.parseColor("#4b4949"));
                txtoneway.setBackgroundResource(R.drawable.white_roundshape);
                txtroundtrip.setTextColor(Color.parseColor("#4b4949"));
                txtroundtrip.setBackgroundResource(R.drawable.white_roundshape);
                Toast.makeText(this, "Hello", Toast.LENGTH_SHORT).show();
                break;

                ///Flight Classicy
            case R.id.economy:
                Economy.setBackgroundResource(R.drawable.blue_roundshape);
                Economy.setTextColor(Color.parseColor("#FFFFFF"));
                Bussiness.setTextColor(Color.parseColor("#4b4949"));
                Bussiness.setBackgroundResource(R.drawable.white_roundshape);
                First.setTextColor(Color.parseColor("#4b4949"));
                First.setBackgroundResource(R.drawable.white_roundshape);
                break;
            case R.id.bussiness:
                Bussiness.setBackgroundResource(R.drawable.blue_roundshape);
                Bussiness.setTextColor(Color.parseColor("#FFFFFF"));
                Economy.setTextColor(Color.parseColor("#4b4949"));
                Economy.setBackgroundResource(R.drawable.white_roundshape);
                First.setTextColor(Color.parseColor("#4b4949"));
                First.setBackgroundResource(R.drawable.white_roundshape);
                break;
            case R.id.frist:
                First.setBackgroundResource(R.drawable.blue_roundshape);
                First.setTextColor(Color.parseColor("#FFFFFF"));
                Bussiness.setTextColor(Color.parseColor("#4b4949"));
                Bussiness.setBackgroundResource(R.drawable.white_roundshape);
                Economy.setTextColor(Color.parseColor("#4b4949"));
                Economy.setBackgroundResource(R.drawable.white_roundshape);
                break;


        }
    }
}
