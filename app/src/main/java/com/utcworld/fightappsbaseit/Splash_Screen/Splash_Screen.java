package com.utcworld.fightappsbaseit.Splash_Screen;

import android.app.ActivityOptions;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.transition.Slide;
import android.view.Gravity;
import android.view.animation.DecelerateInterpolator;

import com.hololo.tutorial.library.Step;
import com.hololo.tutorial.library.TutorialActivity;
import com.utcworld.fightappsbaseit.Activity.Login_Activity;
import com.utcworld.fightappsbaseit.R;

public class Splash_Screen extends  TutorialActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);
        setAnimation();
        setCancelText("Skip");
        setFinishText("OK");
        addFragment(new Step.Builder().setTitle("Booking Now")
                .setContent("Sign up to enjoy amazing offers")
                .setBackgroundColor(Color.parseColor("#FF8B3E")) // int background color
                .setDrawable(R.drawable.screen1) // int top drawable
                .build());

        addFragment(new Step.Builder().setTitle("Flight")
                .setContent("Flight booking then Enjoy Travels  ")
                .setBackgroundColor(Color.parseColor("#FF8B3E")) // int background color
                .setDrawable(R.drawable.screen2) // int top drawable
                .build());
        addFragment(new Step.Builder().setTitle("Booking is Now Easy")

                .setContent("Sign up to enjoy amazing offers")
                .setBackgroundColor(Color.parseColor("#FF8B3E")) // int background color
                .setDrawable(R.drawable.screen3) // int top drawable
                //.setSummary("This is summary")
                .build());


    }

    @Override
    public void currentFragmentPosition(int position) {
        //Toast.makeText(this, "Position : " + position, Toast.LENGTH_SHORT).show();

    }
    @Override
    public void finishTutorial() {
        Intent i = new Intent(this, Login_Activity.class);
        if(Build.VERSION.SDK_INT>20){
            ActivityOptions options =
                    ActivityOptions.makeSceneTransitionAnimation(this);
            startActivity(i,options.toBundle());
        }else {
            startActivity(i);
        }
    }

    public void setAnimation() {
        if (Build.VERSION.SDK_INT > 20) {
            Slide slide = new Slide();
            slide.setSlideEdge(Gravity.LEFT);
            slide.setDuration(400);
            slide.setInterpolator(new DecelerateInterpolator());
            getWindow().setExitTransition(slide);
            getWindow().setEnterTransition(slide);
        }
    }
}
