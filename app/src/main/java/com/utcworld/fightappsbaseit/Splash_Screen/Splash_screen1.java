package com.utcworld.fightappsbaseit.Splash_Screen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.utcworld.fightappsbaseit.R;

public class Splash_screen1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen1);
        final Intent intent=new Intent(Splash_screen1.this,Splash_Screen.class);

        Thread thread=new Thread(){
            public void run(){

                try {
                    sleep(2500);
                }catch (InterruptedException e){
                    e.printStackTrace();

                }
                finally {
                    startActivity(intent);
                    finish();
                }
            }

        };
        thread.start();

    }
}
